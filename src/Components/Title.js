import React from 'react';
import '../css/title.css'


export default function Tilte (props) {
    const {text} = props
    return (
        
        <h4 className="title">
            {text}
        </h4>
        
    )
}