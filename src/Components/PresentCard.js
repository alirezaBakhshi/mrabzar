import React from 'react';
import '../css/present-card.css'


export default function PresentCard(props) {
    const { title, para } = props
    return (
        < div className = "items transition border-radius" >
            <div className = "center header-post">
            <h2>{title}</h2>
            </div>
            <br />
            <div className = "center">
            <p className = "c">
                {para}
                
            </p>
            </div>
        </div >

    )
}