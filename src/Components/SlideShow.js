import React from 'react';
import '../css/slide-show.css'
import {Carousel} from "react-bootstrap"


export default function SlideShow() {
    return (
        <Carousel>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src="https://abzarmart.com/media/slides/193705fef0e3213aa832174bc9f3b628.jpg"
      alt="First slide"
    />
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src="https://abzarmart.com/media/slides/5b882effb1c59a1d351485c4da11b27d.jpg"
      alt="Third slide"
    />
  </Carousel.Item>
</Carousel>
    )
}