import React from 'react';
import '../css/navbar.css'
export default function Navbar () {
    return(
        <div className='navbar'>
            <div className = 'menu'>
                <div className = "menu-item center">فروش ویژه</div>
                <div className = "menu-item center">BBOX دست دوم</div>
                <div className = "menu-item center">فروش همکاران</div>
                <div className = "menu-item center">مشارکت در تامین</div>
                <div className = "menu-item center">مشتری سازمانی</div>
                <div className = "menu-item center">برند ها</div>

            </div>
            <div className = "search center">
                <input type = "text" placeholder = "جستجو" className = "search-input"></input>
            </div>
        </div>
    )
}